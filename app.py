import uuid
import connexion
from connexion import NoContent
import os
from swagger_ui_bundle import swagger_ui_path
import requests
import yaml
import logging.config
from pykafka import KafkaClient
import datetime
import json

with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)
    
logger = logging.getLogger('basicLogger')

with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())

def generate_trace_id():
    trace_id = str(uuid.uuid4())
    return trace_id

def Deposit(body):
    traceid =  generate_trace_id()
    body["trace_id"] = traceid
    logger.info("Received event Deposit request with a unique id of %s"%body["trace_id"] )
    host = str(app_config['events']['hostname'])+":"+ str(app_config['events']['port'])
    headers = { 'content-type': 'application/json' }
    client = KafkaClient(hosts=host)
    topic = client.topics[str.encode(app_config['events']['topic'])]
    producer = topic.get_sync_producer()
    msg = { "type": "deposit",
        "datetime" :
        datetime.datetime.now().strftime(
        "%Y-%m-%dT%H:%M:%S"),
        "payload": body }

    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))
    logger.info("Returned event Deposit request with a unique id of %s"%body["trace_id"] )

    return NoContent, 201


def Withdrawal(body):
    traceid =  generate_trace_id()
    body["trace_id"] = traceid
    logger.info("Received event Withdrawal request with a unique id of %s"%body["trace_id"] )
    host = str(app_config['events']['hostname'])+":"+ str(app_config['events']['port'])
    headers = { 'content-type': 'application/json' }
    client = KafkaClient(hosts=host)
    topic = client.topics[str.encode(app_config['events']['topic'])]
    producer = topic.get_sync_producer()
    msg = { "type": "withdrawal",
        "datetime" :
        datetime.datetime.now().strftime(
        "%Y-%m-%dT%H:%M:%S"),
        "payload": body }

    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))
    logger.info("Returned event Withdrawal request with a unique id of %s"%body["trace_id"] )

    return NoContent, 201

app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yaml", strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    app.run(port=8080)
